package pl.mj.clientspringmysql.dto;

public class AccountDTO {
    private Integer id;
    private String accountNo;
    private Integer userId;
    private Double balance;


    public AccountDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "id=" + id +
                ", accountNo='" + accountNo + '\'' +
                ", userId=" + userId +
                ", balance=" + balance +
                '}';
    }
}
