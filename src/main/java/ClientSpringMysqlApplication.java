import com.google.gson.Gson;
import okhttp3.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.mj.clientspringmysql.dto.AccountDTO;
import pl.mj.clientspringmysql.dto.LoginDTO;
import pl.mj.clientspringmysql.dto.UserDTO;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class ClientSpringMysqlApplication {
    public static UserDTO loggedUser;

    public static void main(String[] args) throws IOException {
        menu();
    }

    private static void menu() throws IOException {
        System.out.println("==============MENU===============");
        System.out.println("1. Logowanie");
        System.out.println("2. Wyjście");
        System.out.println("=================================");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        scanner.nextLine();

        if (choice == 1) {
            //scanner.close();
            login();
        } else if (choice == 2) {
            System.out.println("Zakończono program");
            System.exit(0);
        } else {
            //scanner.close();
            System.out.println("Nieprawidłowa wartość!");
        }
    }

    public static void subMenu() throws IOException {
        System.out.println("==============SUBMENU===============");
        System.out.println("1. Wyświetl saldo kont");
        System.out.println("2. Przejdź do menu głównego");
        System.out.println("=================================");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        scanner.nextLine();

        if (choice == 1) {
            //scanner.close();
            showBalances(loggedUser.getId());
        } else if (choice == 2) {
            menu();
        } else {
            //scanner.close();
            System.out.println("Nieprawidłowa wartość!");
        }
    }

    private static void showBalances(Integer id) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        Request request = new Request.Builder()
                .url("http://localhost:8080/accounts/user/" + id)
                .get()
                .addHeader("Content-Type", "application/json")
                .build();

        Response response = client.newCall(request).execute();

        if (response.code() == 404) {
            System.out.println("Użytkownik nie posiada konta!");
            subMenu();
        }

        String jsonBody = response.body().string();
        Gson gson = new Gson();
        List<AccountDTO> accountDTOS = Arrays.asList(gson.fromJson(jsonBody, AccountDTO[].class));

        int i = 1;
        for (AccountDTO accountDTO : accountDTOS) {
            System.out.println(i + ". Konto: " + accountDTO.getAccountNo());
            System.out.println("Saldo: " + accountDTO.getBalance());
            i++;
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println("Wybierz numer konta:");
        int choice = scanner.nextInt();
        scanner.nextLine();

        choice -= 1;

        try {
            chosenAccount(accountDTOS.get(choice));
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Wybrano nieprawidłowe konto");
            subMenu();
        }
    }

    private static void chosenAccount(AccountDTO accountDTO) {
        System.out.println("Wybrano: " + accountDTO.getAccountNo());
    }

    private static void login() throws IOException {
        Gson gson = new Gson();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj e-mail:");
        String email = scanner.nextLine();
        System.out.println("Podaj hasło:");
        String password = scanner.nextLine();

        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setEmail(email);
        loginDTO.setPassword(password);

//		String jsonBody = gson.toJson(loginDTO);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        stringBuilder.append("\"email\": \"");
        stringBuilder.append(email);
        stringBuilder.append("\", \"password\": \"");
        stringBuilder.append(password);
        stringBuilder.append("\"}");

        String jsonBody = stringBuilder.toString();

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, jsonBody);
        Request request = new Request.Builder()
                .url("http://localhost:8080/users/login")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .build();

        Response response = client.newCall(request).execute();

        int code = response.code();
        if (code == 404 || code == 405) {
            System.out.println("Nie znaleziono użytkownika");
            menu();
        }

        String userJson = response.body().string();
        UserDTO userDTO = gson.fromJson(userJson, UserDTO.class);
        loggedUser = userDTO;

        System.out.println("Witaj " + userDTO.getFirstName());

        subMenu();
    }
}
